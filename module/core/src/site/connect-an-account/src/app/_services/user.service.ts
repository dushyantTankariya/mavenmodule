import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../_model/user';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  baseUrl: string;
  constructor(private http: HttpClient,  @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  getAll() {
    // return this.http.get<User[]>(`/users`);
    return this.http.get<User[]>(this.baseUrl + `/users/`);
  }

  register(user: User) {
    console.log(this.baseUrl);
    // return this.http.post(`/users/register`, user);
    return this.http.post(this.baseUrl + `/users/register`, user);
  }

  delete(id: number) {
    // return this.http.delete(`/users/${id}`);
    return this.http.delete(this.baseUrl + `/users/${id}`);
  }
}
