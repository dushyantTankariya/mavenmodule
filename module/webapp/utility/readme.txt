## steps to prepare webapp 

1. core project > maven clean
2. core project > maven install 

Note: Point 2, command will build chunks and copy it to webapp project using maven plugin


## Additional steps to run on http://localhost:8080 instead of http://localhost:8080/webapp/

1. Edit index.html | update <base href="/webapp/"> to <base href="/">
2. Open Tomcat v9.0 Server
3. Two tabs available at bottom (overview, modules)
4. Select modules > select /webapp > Click on Edit button > update path from "/webapp" to "/"
5. Click on OK button > Press (CTRL + S) to save changes.
6. Run the tomcat server and execute URI http://localhost:8080/ in browser to see implemented changes. 