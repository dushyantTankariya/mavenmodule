package com.snippet.enums;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Base Response Code and status"
 */
public enum BaseResponseEnum {
	/**
	 * @see "http success status message"
	 */
	SUCCESS_STATUS("success"),
	/**
	 * @see "http error status message"
	 */
	ERROR_STATUS("error"),
	/**
	 * @see "http sucess code"
	 */
	CODE_SUCCESS(200),
	/**
	 * @see "http auth failure code"
	 */
	AUTH_FAILURE(401),
	/**
	 * @see "http internal server code"
	 * */
	INTERNAL_SERVER(500),
	/**
	 * @see "http Unprocessable entity"
	 * */
	UNPROCESSABLE_ENTITY(422);
	
	public String value;

	/**
	 * @param value
	 *            of string enums
	 */
	BaseResponseEnum(String value) {
		this.value = value;
	}

	public int intValue;

	/**
	 * @param intValue
	 *            of integer enums
	 */
	BaseResponseEnum(int intValue) {
		this.intValue = intValue;
	}
}
