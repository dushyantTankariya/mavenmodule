package com.snippet.helper;

import static com.snippet.enums.BaseResponseEnum.AUTH_FAILURE;
import static com.snippet.enums.BaseResponseEnum.CODE_SUCCESS;
import static com.snippet.enums.BaseResponseEnum.ERROR_STATUS;
import static com.snippet.enums.BaseResponseEnum.INTERNAL_SERVER;
import static com.snippet.enums.BaseResponseEnum.SUCCESS_STATUS;
import static com.snippet.enums.BaseResponseEnum.UNPROCESSABLE_ENTITY;

import java.util.Calendar;

import org.apache.log4j.Logger;

import com.snippet.rest.UserController;
import com.snippet.vo.AuthenticateVo;
import com.snippet.vo.response.ResponseVo;
import com.snippet.vo.response.impl.BaseResponseVoImpl;
import com.snippet.vo.response.impl.UsersResponseVoImpl;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import static com.snippet.enums.authenticate.*;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "User Helping methods"
 */
public class UserHelper {
	private Logger logger;
	{
		logger = Logger.getLogger(UserController.class);
	}

	/**
	 * @see "Default constructor"
	 */
	public UserHelper() {
	}

	/* =======Authenticate================================================ */
	public ResponseVo buildJwtToken(AuthenticateVo authenticateVo, UsersResponseVoImpl usersResponseVoImpl) {
		logger.trace("buildJwtToken called");
		logger.debug(authenticateVo);
		String token = null;
		
		if(!authenticateVo.isValid()) {
			throw new RuntimeException("Unauthorize");
		}
		
		try {
			//The JWT signature algorithm we will be using to sign the token
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.MINUTE, 3);
			token = Jwts.builder().setSubject("token")
					.setExpiration(calendar.getTime())
					.setIssuer("dushyant.tankariya@gmail.com")
					.claim("groups", new String[] { authenticateVo.getUsername(), authenticateVo.getPassword() })
					// HMAC using SHA-512  and 12345678 base64 encoded
					.signWith(SignatureAlgorithm.HS512, SHA_KEY.value).compact();
			
			logger.trace("buildJwtToken responde");		
		}catch(Exception e) {
			logger.error("Internal Server Error " + e);
			throw new RuntimeException("Internal Server Error");
		}
		
		return getTokenBaseResponse(token, usersResponseVoImpl);
	}
	
	public boolean claim(String token) {
		logger.trace("claim called");
		logger.debug("token=" + token);
		boolean claimFlag = false;
		try {
			Jwts.parser().setSigningKey(SHA_KEY.value).parseClaimsJws(token).getBody();
			claimFlag = true;
		}catch(Exception e) {
			claimFlag = false;
			logger.error("claim failed", e);
		}
		logger.trace("claim responded");
		return claimFlag;
		
	}

	/* =======HTTP RESPONSE=============================================== */
	/**
	 * @return baseResponse {@link ResponseVo} with HTTP success response
	 */
	public ResponseVo getSuccessBaseResponse() {
		BaseResponseVoImpl baseResponseVo = new BaseResponseVoImpl();
		baseResponseVo.setStatus(SUCCESS_STATUS.value);
		baseResponseVo.setCode(CODE_SUCCESS.intValue);
		return baseResponseVo;
	}

	/**
	 * @return baseResponse {@link ResponseVo} with HTTP success response
	 */
	public ResponseVo getTokenBaseResponse(String token, UsersResponseVoImpl authenticateVo) {
		UsersResponseVoImpl baseResponseVo = new UsersResponseVoImpl();
		baseResponseVo.setUsername(authenticateVo.getUsername());
		baseResponseVo.setFirstName(authenticateVo.getFirstName());
		baseResponseVo.setLastName(authenticateVo.getLastName());
		baseResponseVo.setToken(token);
		return baseResponseVo;
	}

	/**
	 * @return baseResponse {@link ResponseVo} with HTTP Authentication failure
	 *         response
	 */
	public ResponseVo getUnprocessableEntityResponse() {
		BaseResponseVoImpl baseResponseVo = new BaseResponseVoImpl();
		baseResponseVo.setStatus(ERROR_STATUS.value);
		baseResponseVo.setCode(UNPROCESSABLE_ENTITY.intValue);
		return baseResponseVo;
	}

	/**
	 * @return baseResponse {@link ResponseVo} with HTTP Authentication failure
	 *         response
	 */
	public ResponseVo getAuthFailureResponse() {
		BaseResponseVoImpl baseResponseVo = new BaseResponseVoImpl();
		baseResponseVo.setStatus(ERROR_STATUS.value);
		baseResponseVo.setCode(AUTH_FAILURE.intValue);
		return baseResponseVo;
	}

	/**
	 * @return baseResponse {@link ResponseVo} with HTTP Internal server failure
	 *         response
	 */
	public ResponseVo getInternalServerFailureResponse() {
		BaseResponseVoImpl baseResponseVo = new BaseResponseVoImpl();
		baseResponseVo.setStatus(ERROR_STATUS.value);
		baseResponseVo.setCode(INTERNAL_SERVER.intValue);
		return baseResponseVo;
	}

}
