package com.snippet.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.snippet.repository.UserRepository;
import com.snippet.repository.impl.UserRepositoryImpl;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Spring configuration includes datasource and jdbc configuration"
 */
@Configuration
@PropertySource(value = { "classpath:datasource.properties" })
public class SpringConfiguration {
	@Autowired
	private Environment env;
	
	@Bean(name = "userRepository")
	UserRepository userRepositoryFactory() {
		return new UserRepositoryImpl();
	}
	
	/**
     * @return configured data source
     */
    @Bean
    DataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getRequiredProperty("jdbc.driverClassName"));
        dataSource.setUrl(env.getRequiredProperty("jdbc.url"));
        dataSource.setUsername(env.getRequiredProperty("jdbc.username"));
        return dataSource;
    }

    /**
     * @param dataSource configured
     * @return jdbcTemplate ready with implemented data source
     */
    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        return jdbcTemplate;
    }
}
