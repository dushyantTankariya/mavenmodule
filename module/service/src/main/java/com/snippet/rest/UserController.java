package com.snippet.rest;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.snippet.config.SpringConfiguration;
import com.snippet.helper.UserHelper;
import com.snippet.repository.UserRepository;
import com.snippet.vo.AuthenticateVo;
import com.snippet.vo.request.UserRegisterRequestVo;
import com.snippet.vo.response.ResponseVo;
import com.snippet.vo.response.impl.UsersResponseVoImpl;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Rest controller for payment"
 */
@RestController
@RequestMapping("/users")
public class UserController {

	private Logger logger;
	{
		logger = Logger.getLogger(UserController.class);
	}
	
	UserController(){}
	
	
	/**
     * @see "To store user details"
     * @param registerRequestVo
     * @return {@link ResponseVo}
     */
	@RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseVo register(@RequestBody UserRegisterRequestVo registerRequestVo) {
    	logger.trace("register called");
    	logger.debug(registerRequestVo);
    	
    	//Declaration
    	AbstractApplicationContext applicationContext;
    	UserRepository userRepository;
    	ResponseVo responseVo;
    	UserHelper userHelper;
    	
    	//Initialization
    	userHelper = new UserHelper();
    	applicationContext= new AnnotationConfigApplicationContext(SpringConfiguration.class);
    	
    	//Process entity
    	try {
    		userRepository = applicationContext.getBean("userRepository",UserRepository.class);
    		userRepository.persist(registerRequestVo);
    		
    		responseVo = userHelper.getSuccessBaseResponse();
    	}catch(DataAccessException e) {
    		responseVo = userHelper.getUnprocessableEntityResponse();
    	}catch(Exception e){
    		responseVo = userHelper.getInternalServerFailureResponse();
		} finally {
			applicationContext.close();
		}
    	
    	//Respond
    	logger.trace("register return");
		return responseVo;
    }
	
	/**
	 * @see "Authenticate user"
	 * @param username of user
	 * @param password of user
	 * @return {@link ResponseVo} with JWT token
	 * */
	@RequestMapping(value="/authenticate", method=RequestMethod.POST)
	public ResponseVo authenticate(@RequestBody AuthenticateVo authenticateVo) {
		logger.trace("authenticate called");
//		logger.debug("authenticate=[username=" + username + ", password=" + password + "]");
		logger.debug(authenticateVo);
		
		//Declaration
    	AbstractApplicationContext applicationContext;
    	UserRepository userRepository;
//    	AuthenticateVo authenticateVo;
    	UsersResponseVoImpl usersResponseVoImpl;
    	ResponseVo responseVo;
    	UserHelper userHelper;
		
    	//Initialization
    	applicationContext= new AnnotationConfigApplicationContext(SpringConfiguration.class);
//    	authenticateVo = new AuthenticateVo(username, password);
    	userHelper = new UserHelper();
    	
    	//Process entity
    	try {
    		userRepository = applicationContext.getBean("userRepository",UserRepository.class);
    		usersResponseVoImpl = userRepository.select(authenticateVo);
    		responseVo = userHelper.buildJwtToken(authenticateVo, usersResponseVoImpl);
    	}catch(RuntimeException e) {
    		responseVo = userHelper.getAuthFailureResponse();
    	}catch(Exception e){
    		responseVo = userHelper.getInternalServerFailureResponse();
		} finally {
			applicationContext.close();
		}
    	
    	//Respond
    	logger.trace("authenticate return");
		return responseVo;
	}
	
	/**
	 * @param token in header
	 * @return List<ResponseVo> 
	 * @see "List of users"
	 * */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<ResponseVo> users(@RequestHeader(value="Authorization") String token) {
		logger.trace("users called");
		token = token.replace("Bearer ", "");
		logger.debug("token = " + token);
		
		//Declaration
		AbstractApplicationContext applicationContext;
    	UserRepository userRepository;
    	List<ResponseVo> list;
    	UserHelper userHelper;
		
    	//Initialization
    	applicationContext= new AnnotationConfigApplicationContext(SpringConfiguration.class);
    	userHelper = new UserHelper();
    	list = new ArrayList<ResponseVo>();
    	
    	//Process entity
    	try {
    		if(userHelper.claim(token)) {
    			userRepository = applicationContext.getBean("userRepository",UserRepository.class);
    			list = new ArrayList<>(	userRepository.list());
    		}else {
    			list.add(userHelper.getAuthFailureResponse());
    			logger.warn("Authentication failure ");
    		}
    	}catch(Exception e) {
    		list.add(userHelper.getInternalServerFailureResponse());
    		logger.error("Internal Server Error ", e);
		} finally {
			applicationContext.close();
		}
		
    	//Respond
		logger.trace("users responded");
		return list;
	}
	
	/**
	 * @see "To delete user"
	 * @param token in header
	 * @param id of user 
	 * */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseVo delete(@RequestHeader(value="Authorization") String token, @PathVariable(value="id") String id) {
		logger.trace("delete called");
		token = token.replace("Bearer ", "");
		logger.debug("token="+ token);
		logger.debug("id=" + id);
		
		//Declaration
		AbstractApplicationContext applicationContext;
    	UserRepository userRepository; 
    	ResponseVo responseVo;
    	UserHelper userHelper;
		
    	//Initialization
    	applicationContext= new AnnotationConfigApplicationContext(SpringConfiguration.class);
    	userHelper = new UserHelper();
    	
    	//Process entity
    	try {
    		if(userHelper.claim(token)) {
    			userRepository = applicationContext.getBean("userRepository",UserRepository.class);
    			if(userRepository.delete(id)) {
    				responseVo = userHelper.getSuccessBaseResponse();
    			}else {
    				responseVo = userHelper.getUnprocessableEntityResponse();
    				logger.warn("Unprocessable Entity");
    			}
    			
    		}else {
    			responseVo = userHelper.getAuthFailureResponse();
    			logger.warn("Authentication failure ");
    		}
    	}catch(Exception e) {
    		responseVo = userHelper.getInternalServerFailureResponse();
    		logger.error("Internal Server Error ", e);
		} finally {
			applicationContext.close();
		}
		
		logger.trace("delete responded");
		return responseVo;
	}
	
	/**
	 * @return "Working" String
	 * @see "GET request test call"
	 * */
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String test() {
		return "User Rest API";
	}
}
