package com.snippet.repository;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.snippet.vo.AuthenticateVo;
import com.snippet.vo.request.UserRegisterRequestVo;
import com.snippet.vo.response.impl.UsersResponseVoImpl;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "User Database Transaction management stub"
 */
public interface UserRepository {
	 /**
     * @param registerRequestVo {@link UserRegisterRequestVo}
     * @see "To register new User"
     * */
    void persist(UserRegisterRequestVo registerRequestVo) throws DataAccessException;
    
    UsersResponseVoImpl select(AuthenticateVo authenticateVo) throws DataAccessException;
    
    List<UsersResponseVoImpl> list() throws DataAccessException;
    
    boolean delete(String id) throws DataAccessException;
}
