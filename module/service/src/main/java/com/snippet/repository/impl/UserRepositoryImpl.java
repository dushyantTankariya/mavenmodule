package com.snippet.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.snippet.repository.UserRepository;
import com.snippet.vo.AuthenticateVo;
import com.snippet.vo.request.UserRegisterRequestVo;
import com.snippet.vo.response.impl.UsersResponseVoImpl;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "User Database Transaction management"
 */
@Repository
public class UserRepositoryImpl implements UserRepository {

	private Logger logger = Logger.getLogger(UserRepositoryImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	/**
	 * @see "Default Constructor"
	 */
	public UserRepositoryImpl() {
	}

	/**
	 * @param registerRequestVo
	 *            {@link UserRegisterRequestVo}
	 * @see "To register new User"
	 */
	@Override
	public void persist(UserRegisterRequestVo registerRequestVo) throws DataAccessException{
		logger.trace("persist called");
		logger.debug(registerRequestVo);
		String sql = "INSERT INTO user (firstName, lastName, username, password) VALUES (?, ?, ?, ?)";
		logger.debug("sql=" + sql);
		jdbcTemplate.update(sql, registerRequestVo.getUserRequestToObjectArrayFactory());
		logger.trace("user Request persist successful");
	}

	@Override
	public UsersResponseVoImpl select(AuthenticateVo authenticateVo) throws DataAccessException {
		logger.trace("select called");
		logger.debug(authenticateVo);
		UsersResponseVoImpl authenticateResultVo  = new UsersResponseVoImpl();
		String sql = "SELECT firstName, lastName, username, password from user where username=? and password=?";
		logger.debug("sql=" + sql);
		authenticateResultVo = jdbcTemplate.queryForObject(
				sql, 
				authenticateVo.getAuthRequestToObjectArrayFactory(),
				new BeanPropertyRowMapper<>(UsersResponseVoImpl.class)
		);
		
		logger.trace("user Request successful Received");
		return authenticateResultVo;
	}

	@Override
	public List<UsersResponseVoImpl> list() throws DataAccessException {
		logger.trace("list called");
		List<UsersResponseVoImpl> usersResponseVoImpls = new ArrayList<UsersResponseVoImpl>();
		String sql = "select id, firstName, lastName, username from user";
		logger.debug("sql=" + sql);
		usersResponseVoImpls = (jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(UsersResponseVoImpl.class)));
		logger.trace("list responded");
		return usersResponseVoImpls;
	}

	@Override
	public boolean delete(String id) throws DataAccessException {
		logger.trace("delete called");
		logger.debug("id=" + id);
		String sql = "delete from user where id=?";
		int i = jdbcTemplate.update(sql, new Object[] {id});
		logger.trace("delete responded");
		return (i>0);
	}
}
