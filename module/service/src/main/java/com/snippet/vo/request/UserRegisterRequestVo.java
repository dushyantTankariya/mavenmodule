package com.snippet.vo.request;

import java.io.Serializable;

import org.springframework.stereotype.Component;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "User Register Request Value Object (Model)"
 */
@Component
public class UserRegisterRequestVo implements Serializable {

	/**
	 * @see "Default Serial Version UID"
	 */
	private static final long serialVersionUID = 1L;
	private String firstName;
	private String lastName;
	private String username;
	private String password;

	/**
	 * @see "Default Constructor"
	 */
	public UserRegisterRequestVo() {

	}

	/**
	 * @param firstName
	 * @param lastName
	 * @param username
	 * @param password
	 * @see "Parameterized constructor"
	 */
	public UserRegisterRequestVo(String firstName, String lastName, String username, String password) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.password = password;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * @return Object[] to return with firstName, lastName, username, and password for Bean Property Row Mapper
	 * */
	public Object[] getUserRequestToObjectArrayFactory() {
		return new Object[] {firstName, lastName, username, password};
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserRegisterRequestVo [firstName=" + firstName + ", lastName=" + lastName + ", username=" + username
				+ ", password=" + password + "]";
	}
}
