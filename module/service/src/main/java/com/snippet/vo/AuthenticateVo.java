package com.snippet.vo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Authnetication details"
 */
public class AuthenticateVo {
	private String username;
	private String password;

	/**
	 * @see "Default Constructor"
	 */
	public AuthenticateVo() {
	}

	/**
	 * @param username
	 * @param password
	 * @see "Parameterized constructor"
	 * @see "Constructor overloading"
	 */
	public AuthenticateVo(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * 
	 * */
	public boolean isValid() {
		return ((username!=null && !username.isEmpty()) && (password!=null && !password.isEmpty()));
	}
	
	/**
	 * @return Object[] to return with username, and password for Bean Property Row Mapper
	 * */
	public Object[] getAuthRequestToObjectArrayFactory() {
		return new Object[] {username, password};
	}
	
	/** 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AuthenticateVo [username=" + username + ", password=" + password + "]";
	}

}
