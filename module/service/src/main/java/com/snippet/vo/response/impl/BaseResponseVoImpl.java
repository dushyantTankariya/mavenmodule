package com.snippet.vo.response.impl;

import org.springframework.stereotype.Component;

import com.snippet.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Base Response"
 * @see {@linkplain com.snippet.vo.response.ResponseVo}
 */
@Component
public class BaseResponseVoImpl implements ResponseVo {
	
	private String status;
	private Integer code;

	/**
	 * @see "Default Constructor"
	 */
	public BaseResponseVoImpl() {
	}

	/**
	 * @param status
	 * @param code
	 * @see "Parameterized constructor"
	 * @see "Constructor overloading"
	 */
	public BaseResponseVoImpl(String status, Integer code) {
		this.status = status;
		this.code = code;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BaseResponseVoImpl [status=" + status + ", code=" + code + "]";
	}

}
