package com.snippet.vo.response.impl;

import org.springframework.stereotype.Component;

import com.snippet.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "User Vo (Model)"
 */
@Component
public class UsersResponseVoImpl implements ResponseVo {

	private Integer id;
	private String username;
	private String firstName;
	private String lastName;
	private String token;

	/**
	 * @see "Default constructor"
	 */
	public UsersResponseVoImpl() {

	}

	/**
	 * @param id
	 * @param username
	 * @param firstName
	 * @param lastName
	 * @param token
	 * @see "Parameterized constructor"
	 * @see "Constructor overloading"
	 */
	public UsersResponseVoImpl(Integer id, String username, String firstName, String lastName, String token) {
		this.id = id;
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.token = token;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token
	 *            the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UsersResponseVoImpl [id=" + id + ", username=" + username + ", firstName=" + firstName + ", lastName="
				+ lastName + ", token=" + token + "]";
	}

}
